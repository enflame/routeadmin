﻿using Routeadmin.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Routeadmin
{
  class GLPI
  {
    static string get_ip(string s_ip)
    {
      string fa = "fixed-address";
      int ind1 = s_ip.LastIndexOf(fa);
      if (ind1 == -1) return null;
      string ip = s_ip.Substring(ind1 + fa.Length, s_ip.Length - fa.Length).Trim();

      return ip;
    }
    public static void get_from_dhcpd(GetSQL.CMySql mysql)
    {
      string[] files = Directory.GetFiles(@"\\dhcp-server.main.tpu.ru\dhcpd");
      foreach (string file in files)
      {
        string ssss = File.ReadAllText(file);
        string[] data = ssss.Split(new Char[] { '{', ';', '}' });

        for (int i = 0; i < data.Length; i++)
        {
          string s_mac = data[i].Trim();
          if ((s_mac.IndexOf("hardware") != -1) & (s_mac.IndexOf("ethernet") != -1))
          {
            try
            {
              string s_ip = data[i + 1].Trim();

              int ind1 = s_mac.LastIndexOf(" ");
              int ind2 = s_mac.Length;// s.LastIndexOf(";");
              string mac = s_mac.Substring(ind1 + 1, ind2 - ind1 - 1).ToLower().Replace(":", "").Trim();
              string ip = get_ip(data[i + 1].Trim());
              if (ip == null) ip = get_ip(data[i + 2].Trim());
              if (ip == null) ip = get_ip(data[i - 1].Trim());

              Reserve.update_ip_by_ip(ip, "", "reserve", mysql);
              Reserve.update_ip_by_mac(ip, mac, "reserve", mysql);
              Reserve.update_ip_by_mac(ip, mac, "reserve_err", mysql);
            }
            catch
            {
              //      break;
            }

          }
        }

      }
      MessageBox.Show("Готово");
    }
    public static void get_from_dhcp(string f_name, GetSQL.CMySql mysql)
    {
       StreamReader reader = new StreamReader(f_name);
      while (!reader.EndOfStream)
      {
        string s = reader.ReadLine().Trim();
        int ind = s.IndexOf("Add reservedip");
        if (ind != -1)
        {
          s = s.Substring(ind);
          s = s.Replace("  ", " ");
          s = s.Replace("  ", " ");
          s = s.Replace("\"", "");
          string[] str = s.Split(new char[] { ' ' });
          {
            string ip = str[2];
            string m = str[3];
            string mac = str[3].Replace("-", "");
            string name = str[4];
            if (name == "-U-" && str.Length > 8) name = str[8];

            Reserve.update_ip_by_mac(ip, mac, "reserve", mysql);
            Reserve.update_ip_by_mac(ip, mac, "reserve_err", mysql);
          }
        }
      }

    }
    public static void update_glpi(bool isPrinters, GetSQL.CMySql mysql)
    {
      string device_id = "";
      string[] glpi = null;
      if (isPrinters)
      {
        device_id = "2";
        glpi = File.ReadAllLines(@"c:\temp\glpi_pr.csv");
      }
      else
      {
        device_id = "1";
        glpi = File.ReadAllLines(@"c:\temp\glpi_pc.csv");

      }


      int col_count = glpi[0].Split(new char[] { ';' }).Length - 1;

      int i = 0;
      while (i < glpi.Length)
      {
        try
        {

          List<string> arr = new List<string>();// glpi[i].Replace("\"", "").Split(new char[] { ';' });
          arr.AddRange(glpi[i].Replace("\"", "").Split(new char[] { ';' }));
          arr.RemoveAt(arr.Count - 1);
          while (arr.Count < col_count)
          {
            i++;
            string[] arr1 = glpi[i].Replace("\"", "").Split(new char[] { ';' });
            arr.AddRange(arr1);
            arr.RemoveAt(arr.Count - 1);
          }
          if (arr.Count > col_count)
          {
            MessageBox.Show("Error \r\n" + arr[i]);
            return;
          }

          string gsynch = "0";
          int is_model = 0;
          int is_mo = 0;
          string glpi_comm = "";
          string glpi_status = "";
          string glpi_inv_num = "";
          if (isPrinters)
          {
            if (!String.IsNullOrEmpty(arr[6])) is_model = 1;
            if (!String.IsNullOrEmpty(arr[9])) is_mo = 1;
            glpi_comm = arr[13];
            glpi_status = arr[3];
            glpi_inv_num = arr[6];


            arr[2] = arr[2].Replace("\"", "");
            string[] ba = arr[2].Split(new char[] { '-' });
            if (ba.Length == 2)
            {
              string gb = ba[0];
              string ga = ba[1];
              string gname = arr[0];
              string gid = arr[1].Replace(" ", "");
              string gmac = arr[16];
              gmac = gmac.Replace(":", "").ToLower();

              string a_id = DB.Place.check_place(ga, gb, mysql);
              DB.Reserve.check_glpi(gid, gname, gmac, gsynch, a_id, device_id, is_model, is_mo, glpi_status, glpi_comm, glpi_inv_num, mysql);
            }
          }
          else
          {
            if (arr[14].IndexOf("ОК") >= 0) gsynch = "1";
            if (!String.IsNullOrEmpty(arr[9])) is_model = 1;
            if (!String.IsNullOrEmpty(arr[16])) is_mo = 1;
            glpi_comm = arr[17];
            glpi_status = arr[4];
            glpi_inv_num = arr[5];

            arr[3] = arr[3].Replace("\"", "");
            string[] ba = arr[3].Split(new char[] { '-' });
            if (ba.Length == 2)
            {
              string gb = ba[0];
              string ga = ba[1];
              string gname = arr[0];
              string gid = arr[1].Replace(" ", "");
              string gmac = arr[15];
              gmac = gmac.Replace(":", "").ToLower();

              string a_id = DB.Place.check_place(ga, gb, mysql);
              DB.Reserve.check_glpi(gid, gname, gmac, gsynch, a_id, device_id, is_model, is_mo, glpi_status, glpi_comm, glpi_inv_num, mysql);
            }
          }


        }
        catch (Exception exc)
        {
          if (MessageBox.Show(exc.Message + ": \r\n Продолжить?", "Ошибка", MessageBoxButton.YesNo) == MessageBoxResult.No) ;
          //          return;
        }
        i++;
      }
      MessageBox.Show("Готово");

    }
  }
}
