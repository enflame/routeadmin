﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using System.Resources;

namespace Routeadmin.DB
{
  class Swinfo
  {
    public static DataTable getAll()
    {
      MySqlConnection conn;
      ResourceManager rm = new ResourceManager("Routeadmin.Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());
      conn = new MySqlConnection(rm.GetString("connection_string"));
      conn.Open();

      string sql = "SELECT * FROM swinfo where status=1  order by id";

      DataSet ds = new DataSet();
      MySqlCommand command = new MySqlCommand(sql, conn);
      MySqlDataAdapter DA = new MySqlDataAdapter();
      command.Connection = conn;
      command.CommandText = sql;
      DA.SelectCommand = command;
      DA.TableMappings.Add("swinfo", "swinfo");
      DA.Fill(ds);
      return ds.Tables[0];
    }
  }
}
