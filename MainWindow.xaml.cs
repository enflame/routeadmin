﻿using System.Windows;


using System;
using System.IO;
using System.Threading;
using System.Data;
using System.Collections.Generic;
using Routeadmin.DB;
using System.Resources;

namespace Routeadmin
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
     FindPorts ports;
    GetSQL.CMySql mysql = new GetSQL.CMySql();
 
    public MainWindow()
    {
      try
      {
        ResourceManager rm = new ResourceManager("Routeadmin.Properties.Resources", System.Reflection.Assembly.GetExecutingAssembly());
         mysql.ConnectToDB(rm.GetString("connection_string"), GetSQL.CMySql.DB_MYSQL);
      }
      catch (Exception exc)
      {
        MessageBox.Show(exc.Message);
        Close();
      }
      InitializeComponent();
    }
    void FindPortsEvent(object sender, FindPorts.FindPortsEventArgs args)
    {
      lb_log.Dispatcher.Invoke(new ThreadStart(delegate
      {
        if (lb_log.Items.Count > 300) lb_log.Items.Clear();
        lb_log.Items.Add(args.obj);
        lb_log.SelectedIndex = lb_log.Items.Count - 1;
        lb_log.SelectedItem = lb_log.Items[lb_log.Items.Count - 1];

      }));
    }
 

    private void Window_Closed(object sender, EventArgs e)
    {
      if (ports != null)
        ports.Stop();
    }

    private void b_from_glpi_Click(object sender, RoutedEventArgs e)
    {
      GLPI.update_glpi(false, mysql);
    }
    private void b_from_glpi_printers_Click(object sender, RoutedEventArgs e)
    {
      GLPI.update_glpi(true, mysql);
    }

    private void b_from_dhcpd_Click(object sender, RoutedEventArgs e)
    {
      GLPI.get_from_dhcpd(mysql);
    }

    private void b_start_macs_Click(object sender, RoutedEventArgs e)
    {
      ports = new FindPorts();
      ports.FindPortsEvent += new FindPorts.FindPortsEventHandler(FindPortsEvent);
    }

  }
}
