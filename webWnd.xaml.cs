﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Routeadmin
{
  /// <summary>
  /// Логика взаимодействия для webWnd.xaml
  /// </summary>
  public partial class webWnd : Window
  {
    public bool isStart = false;
    public webWnd(Awesomium.Windows.Controls.WebControl web)
    {
      InitializeComponent();
      main_grid.Children.Add(web);
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      isStart = true;
      b_start.IsEnabled = false;
    }

    private void Window_Closed(object sender, EventArgs e)
    {

    }
  }
}
