﻿using System;
using System.Threading;
using System.Data;
using Routeadmin.DB;

namespace Routeadmin
{
  public class FindPorts
  {
    bool IsWorking = true;


    bool IsGetDashBoard = false;
    bool IsLoading = false;


    Awesomium.Windows.Controls.WebControl WebControl;
    DataTable dt_swinfo;
    webWnd wnd;

    public bool isClosed = false;
    public FindPorts()
    {
      WebControl = new Awesomium.Windows.Controls.WebControl();
      WebControl.Height = 600;
      WebControl.LoadingFrameComplete += AweWeb_LoadingFrameComplete;
      try
      {
        dt_swinfo = Swinfo.getAll();
      }
      catch (Exception exc)
      {
        throw exc;
      }
      IsGetDashBoard = false;
      wnd = new webWnd(WebControl);
      wnd.Show();
      WebControl.Source = new Uri("http://route.enin.tpu.ru/site/index");
      StartServer();
    }
    public void Stop()
    {
      IsWorking = false;
    }
    private void AweWeb_LoadingFrameComplete(object sender, Awesomium.Core.FrameEventArgs e)
    {
      if (e.IsMainFrame)
      {
        IsLoading = false;
        string html = WebControl.HTML;
        if (string.IsNullOrEmpty(html)) return;
        if (IsGetDashBoard)
        {
          IsGetDashBoard = false;
        }
      }
    }

    void GetBids(string url)
    {
      IsGetDashBoard = true;
      Navigate(url);
      while (IsGetDashBoard) Thread.Sleep(500);
    }
    void UpdatePorts()
    {
      while (!wnd.isStart) Thread.Sleep(100);
      while (true)
      {
        foreach (DataRow dr in dt_swinfo.Rows)
        {
          if (!IsWorking) return;
          OnFindPortsEvent(new FindPortsEventArgs { obj = " исследуется коммутатор " + dr["sw_name"].ToString(), State = 0 });

          GetBids("http://route.enin.tpu.ru/swports/findswport?host=" + dr["id"].ToString() + "&issw=1");

        }

        TimeSpan ts = new TimeSpan(1, 0, 0);
        DateTime dt1 = DateTime.Now;
        DateTime dt2 = DateTime.Now;
        FindPortsEventArgs args = new FindPortsEventArgs{ obj = " Последнее обновление было в " + dt2.ToShortTimeString(), State = 0 };
        OnFindPortsEvent(args);
        while ((dt2 - dt1) < ts)
        {
          Thread.Sleep(60000);
          dt2 = DateTime.Now;
          if (!IsWorking) return;
        }



      }
    }

    public void StartServer()
    {
      Thread th = new Thread(UpdatePorts);
      //  Thread th = new Thread(Test);
      th.Start();
    }
    void Navigate(string url, bool isConnect = false)
    {
      if (!isConnect) IsLoading = true;
      WebControl.Dispatcher.Invoke(new ThreadStart(delegate
      {
        WebControl.Source = new Uri(url);

      }));
      while (IsLoading) Thread.Sleep(1000);
      Thread.Sleep(5000);
    }
    void Test()
    {
      while (true)
      {
        Navigate("https://www.gosuslugi.ru/");
        Navigate("https://www.google.ru/");
        Navigate("https://www.tpu.ru/");
        Navigate("https://www.ya.ru/");
      }
    }

    public void Connect()
    {
      Navigate("https://help.tpu.ru/otrs/index.pl?Action=PreLogin&RequestedURL=Action%3DAgentDashboard", true);
    }

    #region обработка событий
    public class FindPortsEventArgs : EventArgs
    {
      public int State;
      public object obj;

      public FindPortsEventArgs()
      {
        State = -1;
      }
    }

    public delegate void FindPortsEventHandler(Object sender, FindPortsEventArgs args);
    public event FindPortsEventHandler FindPortsEvent;
    protected virtual void OnFindPortsEvent(FindPortsEventArgs e)
    {
      FindPortsEvent?.Invoke(this, e);
    }

    #endregion

  }
}
